### VIRTUALENV ###

* mkvirtualenv -p python3.5
* cd /path/to/your/project
* pip install -r req.txt

### DATABASE ###

* In path/to/your/project/testlib/settings.py check line 84. Path should be correct.
* Go to mysql console
* mysql> CREATE DATABASE testlib;
* mysql>\q

* cd /path/to/your/project
* nano db.conf  #  change user and password for your db user